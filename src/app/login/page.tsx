import Link from "next/link"
import Image from 'next/image'
import logo from '/src/images/csilogo.png'

const Page = () => {
    return (
        <div className="relative flex flex-col items-center justify-center min-h-screen overflow-hidden">
      <div className="w-full p-6 bg-white rounded-md shadow-md lg:max-w-xl">
        <div className="flex justify-center">
      <Image
                  src={logo}
                  alt="csi logo"
                  width={393}
                  height={105}
                />
        </div>
        <form className="mt-6">
          <div className="mb-4">
            <label
              htmlFor="email"
              className="block text-sm font-semibold text-gray-800"
            >
              Username
            </label>
            <input
              type="text"
              className="block w-full px-4 py-2 mt-2 text-gray-700 bg-white border rounded-md focus:border-gray-400 focus:ring-gray-300 focus:outline-none focus:ring focus:ring-opacity-40"
            />
          </div>
          <div className="mb-2">
            <label
              htmlFor="password"
              className="block text-sm font-semibold text-gray-800"
            >
              Password
            </label>
            <input
              type="password"
              className="block w-full px-4 py-2 mt-2 text-gray-700 bg-white border rounded-md focus:border-gray-400 focus:ring-gray-300 focus:outline-none focus:ring focus:ring-opacity-40"
            />
          </div>
          <div className="mt-2" >
          <Link href="/main">
            <button className="w-full px-4 py-2 tracking-wide text-white transition-colors duration-200 transform bg-neutral-600 rounded-md hover:bg-neutral-500 focus:outline-none focus:bg-gray-600 mt-5">
              Login
            </button>
            </Link>
          </div>
        </form>
      </div>
    </div>
    )
  }
  
  export default Page

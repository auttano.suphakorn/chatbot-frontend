"use client"
import {HomeIcon, PlusCircleIcon, PencilIcon, ComputerDesktopIcon, ArrowUpTrayIcon, Cog6ToothIcon, ArrowRightStartOnRectangleIcon } from "@heroicons/react/24/outline"
import Link from "next/link"
import { useSelectedLayoutSegment } from "next/navigation"
import { useState } from "react"
import { text } from "stream/consumers"
import Image from 'next/image'
import logo from '/src/images/csilogo.png'
function classNames(...classes: string[]){
  return classes.filter(Boolean).join(' ')
}

const Sidebar = () => {
  const segment = useSelectedLayoutSegment()
  const sidebaroption = [
    {name: "Home", href:"/main", icon: HomeIcon, current: !segment ? true : false},
    {name: "Training", href:"/main/training", icon: PlusCircleIcon, current: `/${segment}` === "/training" ? true : false},
    {name: "PromtEdit", href:"/main/promt-edit", icon: PencilIcon, current: `/${segment}` === "/promt-edit" ? true : false},
    {name: "Testing", href:"/main/testing", icon: ComputerDesktopIcon, current: `/${segment}` === "/testing" ? true : false},
    {name: "Deployment", href:"/main/deployment", icon: ArrowUpTrayIcon, current: `/${segment}` === "/deployment" ? true : false},
    {name: "Settings", href:"/main/settings", icon: Cog6ToothIcon, current: `/${segment}` === "/settings" ? true : false},
  ]

  
  return ( 
    <div className="p-3">
    <div className="hidden lg:fixed lg:inset-y-0 lg:inset-x-0 lg:flex lg:w-72 lg:flex-col">
        <div  style={{ backgroundColor: '#404040' }} className="flex grow flex-col -gapy-y-5 overflow-y-auto px-6 pb-4 border-r-1">
        <Link href="/main">
            <div className="flex h-16 shrink-0 item-c enter mb-7 mt-3">
                <Image
                  src={logo}
                  alt="csi logo"
                  width={393}
                  height={105}
                />
            </div>
            </Link>
              <nav className="flex flex-1 flex-col">
                <ul role="list" className="flex flex-1 flex-col gap-y-7">
                  <ul role="list" className="-mx-2 space-y-1">
                    {
                      sidebaroption.map((option) => (
                        <li key={option.name}>
                          <Link href={option.href} className={classNames(option.current ? "bg-neutral-500" : "hover:bg-neutral-500" ,"group flex gap-x-3 rounded-full p-1 text-sm leading-6 font-semibold")}>
                            <option.icon className="h-6 w-6 shrink-0" />
                            {option.name}
                          </Link>
                        </li>
                      ))
                    }
                  </ul>
                </ul>
            </nav>
            <div className="text-center">Admin</div>
            <div>
            <nav className="flex flex-1 flex-col">
                <ul role="list" className="flex flex-1 flex-col gap-y-7">
                  <ul role="list" className="-mx-2 space-y-1">
                    {
                        <li className="bg-red-600 rounded-full">
                          <Link href={"/login"} className={classNames("bg-red","group flex gap-x-3 rounded-full p-1 text-sm leading-6 font-semibold")}>
                          <ArrowRightStartOnRectangleIcon name="Logout" className="h-6 w-6 shrink-0"/>
                            Logout
                          </Link>
                        </li>
                    }
                  </ul>
                </ul>
            </nav>
            </div>
          </div>
        </div>
    </div>
  )
}

export default Sidebar 